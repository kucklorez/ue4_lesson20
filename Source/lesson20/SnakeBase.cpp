// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Food.h"
#include "Bonus.h"
#include <ctime>
#include <cmath>

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MovementSpeed = 10.f;
	ElementSize = 100.f;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();

	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);	

	std::srand(std::time(nullptr));
	CreateFoodActor();
	CreateBonusActor();
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	if ((SnakeElements.Num() > 0) && (ElementsNum == 1))
	{
		FTransform LastElementTransform(SnakeElements[SnakeElements.Num() - 1]->GetActorLocation());
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, LastElementTransform);
		NewSnakeElem->SnakeOwner = this;
		SnakeElements.Add(NewSnakeElem);
	}
	else
	{
		for (int i = 0; i < ElementsNum; i++)
		{
			FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
			FTransform NewTransform(NewLocation);
			ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
			NewSnakeElem->SnakeOwner = this;
			int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
			if (ElemIndex == 0)
			{
				NewSnakeElem->SetFirstElementType();
			}
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);

	switch (LastMoveDirection)
	{
		
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;

	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;

	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;

	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{

	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;

		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
		else
		{
			this->Destroy();
		}
	}
}

void ASnakeBase::CreateFoodActor()
{
	FVector NewLocation;
	do {
		NewLocation.X = (std::rand() % 15 - 7) * ElementSize;
		NewLocation.Y = (std::rand() % 15 - 7) * ElementSize;
		NewLocation.Z = 0.f;
	} while (isTooClose(NewLocation, true));

	FTransform NewTransform(NewLocation);
	
	FoodActor = GetWorld()->SpawnActor<AFood>(FoodActorClass, NewTransform);
}

void ASnakeBase::CreateBonusActor()
{
	int BonusType = std::rand() % 5; //chance to spawn bonus is 60% 
	if (BonusType > 0 && BonusType < 4)
	{
		FVector NewLocation;
		do {
			NewLocation.X = (std::rand() % 15 - 7) * ElementSize;
			NewLocation.Y = (std::rand() % 15 - 7) * ElementSize;
			NewLocation.Z = 0.f;
		} while (isTooClose(NewLocation));
		FTransform NewTransform(NewLocation);

		ABonus *BonusActor = GetWorld()->SpawnActor<ABonus>(BonusActorClass, NewTransform);
		if (IsValid(BonusActor))
		{
			BonusActor->BonusType = BonusType;
			BonusActor->SetBonusMaterial();
		}
	}

}

void ASnakeBase::ApplyBonus(int CurentBonusType)
{
	switch (CurentBonusType)
	{
	case 1: 
		MovementSpeed *= 0.75f;
		SetActorTickInterval(MovementSpeed);
		break;

	case 2: 
		MovementSpeed *= 1.5f;
		SetActorTickInterval(MovementSpeed);
		break;

	case 3:
		if (SnakeElements.Num() > 1)
		{
			SnakeElements.Pop()->Destroy();
		}
		break;
	}

}

bool ASnakeBase::isTooClose(FVector& SomeLocation, bool isFood)
{
	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		FVector ElementLocation = SnakeElements[i]->GetActorLocation();
		if ((std::abs(ElementLocation.X - SomeLocation.X) < ElementSize) && (std::abs(ElementLocation.Y - SomeLocation.Y) < ElementSize))
		{
			return true;
		}
	}

	if (!isFood)
	{
		if (IsValid(FoodActor))
		{
			FVector FoodLocation = FoodActor->GetActorLocation();
			if ((std::abs(FoodLocation.X - SomeLocation.X) < ElementSize) && (std::abs(FoodLocation.Y - SomeLocation.Y) < ElementSize))
			{
				return true;
			}
		}
	}

	return false;
}



